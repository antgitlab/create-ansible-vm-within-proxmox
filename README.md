This requires ansible in order to create an ansible controller within proxmox.
The proxmox instance needs proxmoxer and requests installed first

# This sometimes needs to be run twice
ansible-playbook stopAndDeleteVM.yml

ansible-playbook createAndStartVM.yml

# Need to find vmip by logging in
# Need to do manual ssh-copy-id as in ssh-copy-id 192.168.0.216
# Need to update inventory with vmid and vmip

ansible-playbook main.yml

On server once created

ssh-copy-id 192.168.0.70
ssh-copy-id 192.168.0.203
ssh-copy-id 192.168.0.207
ssh-copy-id 192.168.0.219
ssh-copy-id 192.168.0.233
ssh-copy-id 192.168.0.252

ssh-copy-id root@192.168.0.120

ssh-copy-id root@192.168.0.70
ssh-copy-id root@192.168.0.203
ssh-copy-id root@192.168.0.207
ssh-copy-id root@192.168.0.219
ssh-copy-id root@192.168.0.233
ssh-copy-id root@192.168.0.252